import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {DtpMainPageComponent} from "./dtp/dtp-main-page/dtp-main-page.component";
import {DtpEditorComponent} from "./dtp/dtp-editor/dtp-editor.component";

const routes: Routes = [
  { path: '',   redirectTo: '/dtp', pathMatch: 'full' },
  {
    path: 'dtp',
    component: DtpMainPageComponent
  },
  {
    path: 'dtp/editor',
    component: DtpEditorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
