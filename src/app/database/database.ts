import Dexie from "dexie";
import {DtpModelInterface} from "../models/interfaces/dtpModel.interface";
import {DtpSectionDtpModelInterface} from "../models/interfaces/dtpSectionDtpModel.interface";
import {DtpParagraphInterface} from "../models/interfaces/dtpParagraph.interface";

export class Database extends Dexie {
  dtpModels: Dexie.Table<DtpModelInterface, number>;
  sectionDtpModels: Dexie.Table<DtpSectionDtpModelInterface, number>;
  paragraphModels: Dexie.Table<DtpParagraphInterface, number>;

  constructor() {
    super("Database");

    this.version(1).stores({
      dtpModels: '++id, title',
      sectionDtpModels: '++id, content, order, parent, dtpModel',
      paragraphModels: '++id, content, order, parent, dtpModel, tags',
    });

    this.dtpModels = this.table("dtpModels");
    this.sectionDtpModels = this.table("sectionDtpModels");
    this.paragraphModels = this.table("paragraphModels");
  }
}
