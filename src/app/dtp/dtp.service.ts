import { Injectable } from '@angular/core';
import {Database} from "../database/database";
import {DtpModelInterface} from "../models/interfaces/dtpModel.interface";
import {DtpModel} from "../models/dtp.model";

@Injectable({
  providedIn: 'root'
})
export class DtpService {

  public selectedDtp: DtpModelInterface | undefined;

  private _db = new Database();

  constructor() {}

  public async getAllDTPModels(): Promise<DtpModelInterface[]> {
    return this._db.dtpModels.toArray().then(dtps => dtps.map(dtp => new DtpModel(dtp.title, dtp.id)));
  }
}
