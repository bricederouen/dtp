import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DtpMainPageComponent } from './dtp-main-page.component';

describe('DtpMainPageComponent', () => {
  let component: DtpMainPageComponent;
  let fixture: ComponentFixture<DtpMainPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DtpMainPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DtpMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
