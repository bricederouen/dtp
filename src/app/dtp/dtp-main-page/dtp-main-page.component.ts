import { Component, OnInit } from '@angular/core';
import {DtpService} from "../dtp.service";
import {DtpModelInterface} from "../../models/interfaces/dtpModel.interface";
import {DtpModel} from "../../models/dtp.model";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-dtp-main-page',
  templateUrl: './dtp-main-page.component.html',
  styleUrls: ['./dtp-main-page.component.scss']
})
export class DtpMainPageComponent implements OnInit {

  public dtps: DtpModelInterface[] | undefined;

  constructor(private _dtpService: DtpService, private _router: Router, private _route: ActivatedRoute) { }

  async ngOnInit(): Promise<void> {
    this.dtps = await this._dtpService.getAllDTPModels();
  }

  public selectDTP(dtp: DtpModelInterface) {
    this._dtpService.selectedDtp = dtp;
    this.goToEditor();
  }

  public createDTP() {
    this._dtpService.selectedDtp = new DtpModel('My brand new DTP :-)!');
    this.goToEditor();
  }

  private goToEditor() {
    this._router.navigate(['editor'], {relativeTo: this._route});
  }

}
