import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DtpEditorComponent } from './dtp-editor.component';

describe('DtpEditorComponent', () => {
  let component: DtpEditorComponent;
  let fixture: ComponentFixture<DtpEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DtpEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DtpEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
