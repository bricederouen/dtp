import {Component, OnInit, TemplateRef} from '@angular/core';
import {DtpModelInterface} from "../../models/interfaces/dtpModel.interface";
import {DtpService} from "../dtp.service";
import {DtpSectionDtpModelInterface} from "../../models/interfaces/dtpSectionDtpModel.interface";
import {SectionDtpModel} from "../../models/section-dtp.model";
import {ParagraphModel} from "../../models/paragraph.model";
import {TagsEnum} from "../../models/enums/tags.enum";

@Component({
  selector: 'app-dtp-editor',
  templateUrl: './dtp-editor.component.html',
  styleUrls: ['./dtp-editor.component.scss']
})
export class DtpEditorComponent implements OnInit {

  public dtp: DtpModelInterface | undefined;
  public displayedTemplate: TemplateRef<any> | undefined;
  public tagsList: string[] = Object.keys(TagsEnum).filter(value => !isNaN(Number(value))).map((key: any) => TagsEnum[key]);
  public tagsEnum = TagsEnum;

  private _mapElementToChildren: Map<DtpModelInterface | DtpSectionDtpModelInterface, DtpSectionDtpModelInterface[] | undefined> =
    new Map<DtpModelInterface | DtpSectionDtpModelInterface, DtpSectionDtpModelInterface[] | undefined>();
  private _timer: any;

  constructor(private _dtpService: DtpService) { }

  async ngOnInit() {
    this.dtp = this._dtpService.selectedDtp;
  }

  saveChanges(model: DtpModelInterface | DtpSectionDtpModelInterface) {
    clearTimeout(this._timer);
    this._timer = setTimeout(() => model?.save(), 500);
  }

  async createSectionDtp(model: DtpModelInterface | DtpSectionDtpModelInterface, order: number) {
    if (model && this.dtp) {
      await this.dtp.save();
      await model.save();
      const sectionDtp = new SectionDtpModel('My wonderful section', this.dtp, order, model === this.dtp ? undefined : <DtpSectionDtpModelInterface>model);
      await this.reorderAndSave((await model.getChildren()).concat(sectionDtp));
      this.refreshChildren(model);
    }
  }

  async createParagraph(model: DtpModelInterface | DtpSectionDtpModelInterface, order: number) {
    if (model && this.dtp) {
      await this.dtp.save();
      await model.save();
      const paragraphModel = new ParagraphModel('My wonderful paragraph', this.dtp, order, [],model === this.dtp ? undefined : <DtpSectionDtpModelInterface>model);
      await this.reorderAndSave((await model.getChildren()).concat(paragraphModel));
      this.refreshChildren(model);
    }
  }

  async reorderAndSave(models: DtpSectionDtpModelInterface[]) {
    return Promise.all(this.reorder(models)
      .map((orderedModel, index) => {orderedModel.order = index; return orderedModel.save();}));
  }

  reorder(models: DtpSectionDtpModelInterface[]): DtpSectionDtpModelInterface[] {
    return models.sort((a, b) => a.order > b.order ? 1 : -1);
  }

  getChildren(model: DtpModelInterface | DtpSectionDtpModelInterface) {
    if (this._mapElementToChildren.has(model)) {
      return this._mapElementToChildren.get(model);
    }
    this._mapElementToChildren.set(model, undefined);
    this.refreshChildren(model);
    return this._mapElementToChildren.get(model);
  }

  refreshChildren(model: DtpModelInterface | DtpSectionDtpModelInterface) {
    model.getChildren().then(children => {
      this._mapElementToChildren.set(model, this.reorder(children));
    });
  }

  hoverTrigger(templateRef: TemplateRef<any>) {
    this.displayedTemplate = templateRef;
  }

  isParagraph(model: DtpSectionDtpModelInterface) {
    // @ts-ignore
    return model['tags'] !== undefined;
  }

  getTagsValue(tagText: any) {
    return TagsEnum[tagText];
  }

  async deleteModel(model: DtpSectionDtpModelInterface) {
    await model.delete();
    this.refreshChildren(model.parent ?? model.dtpModel);
  }

}
