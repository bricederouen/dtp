import {DtpParagraphInterface} from "./interfaces/dtpParagraph.interface";
import {DtpSectionDtpModelInterface} from "./interfaces/dtpSectionDtpModel.interface";
import {TagsEnum} from "./enums/tags.enum";
import {DtpModelInterface} from "./interfaces/dtpModel.interface";
import {Database} from "../database/database";
import {SectionDtpModel} from "./section-dtp.model";

export class ParagraphModel implements DtpParagraphInterface {
  public id: number | undefined;
  public content: string;
  public dtpModel: DtpModelInterface;
  public order: number;
  public parent: DtpSectionDtpModelInterface | undefined;
  private _tags: TagsEnum[];

  private _db = new Database();

  constructor(content: string, dtpModel: DtpModelInterface, order: number, tags: TagsEnum[], parent?: DtpSectionDtpModelInterface, id?: number) {
    this.content = content;
    this.dtpModel = dtpModel;
    this.order = order;
    this.parent = parent;
    this._tags = tags;
    this.tags = tags;
    this.id = id;
  }

  save(): Promise<void> {
    return this._db.transaction('rw', this._db.paragraphModels,async() => {
      // @ts-ignore
      this.id = await this._db.paragraphModels.put(<DtpParagraphInterface>this._serialize());
    });
  }

  async delete(): Promise<void> {
    return this._db.transaction('rw', this._db.paragraphModels, async () => {
      if (this.id !== undefined) {
        this.tags = [];
        this._db.paragraphModels.where('id').equals(this.id).delete();
      }
    });
  }

  private _serialize() {
    return {
      content: this.content,
      dtpModel: this.dtpModel.id,
      order: this.order,
      parent: this.parent?.id ?? undefined,
      tags: this._tags,
      id: this.id
    }
  }

  getChildren(): Promise<DtpSectionDtpModelInterface[]> {
    return Promise.resolve([]);
  }

  getTags(): readonly TagsEnum[] {
    return this._tags;
  }

  set tags(value: TagsEnum[]) {
    this._tags = value;
    if (this.parent && this.parent instanceof SectionDtpModel) {
      this.parent.setTags(this, this._tags);
    }
  }

  get tags(): TagsEnum[] {
    return this._tags;
  }
}
