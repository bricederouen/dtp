import {DtpSectionDtpModelInterface} from "./dtpSectionDtpModel.interface";

export interface DtpModelInterface {
  id?: number;
  title: string;
  save: () => Promise<void>;
  delete: () => Promise<void>;
  getChildren: () => Promise<DtpSectionDtpModelInterface[]>;
}
