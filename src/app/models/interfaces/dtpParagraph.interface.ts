import {DtpSectionDtpModelInterface} from "./dtpSectionDtpModel.interface";
import {TagsEnum} from "../enums/tags.enum";

export interface DtpParagraphInterface extends DtpSectionDtpModelInterface {
  tags: TagsEnum[];
}
