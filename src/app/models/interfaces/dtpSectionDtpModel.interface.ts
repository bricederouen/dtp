import {DtpModelInterface} from "./dtpModel.interface";
import {TagsEnum} from "../enums/tags.enum";

export interface DtpSectionDtpModelInterface {
  id?: number;
  content: string;
  order: number;
  parent?: DtpSectionDtpModelInterface;
  dtpModel: DtpModelInterface;
  save: () => Promise<void>;
  delete: () => Promise<void>;
  getChildren: () => Promise<DtpSectionDtpModelInterface[]>;
  getTags: () => readonly TagsEnum[]
}
