import {DtpSectionDtpModelInterface} from "./interfaces/dtpSectionDtpModel.interface";
import {DtpModelInterface} from "./interfaces/dtpModel.interface";
import {Database} from "../database/database";
import {ParagraphModel} from "./paragraph.model";
import {TagsEnum} from "./enums/tags.enum";

export class SectionDtpModel implements DtpSectionDtpModelInterface {

  public id: number | undefined;
  public content: string;
  public dtpModel: DtpModelInterface;
  public order: number;
  public parent: DtpSectionDtpModelInterface | undefined;

  private _db = new Database();
  private _mapTags: Map<DtpSectionDtpModelInterface, TagsEnum[]> = new Map<DtpSectionDtpModelInterface, TagsEnum[]>();
  private _tags: TagsEnum[] | undefined = [];

  constructor(content: string, dtpModel: DtpModelInterface, order: number, parent?: DtpSectionDtpModelInterface, id?: number) {
    this.content = content;
    this.dtpModel = dtpModel;
    this.order = order;
    this.parent = parent;
    this.id = id;
  }

  async getChildren(): Promise<DtpSectionDtpModelInterface[]> {
    if (this.id) {
      const promise = Promise.all([
        this._db.sectionDtpModels.where('parent').equals(this.id).toArray(),
        this._db.paragraphModels.where('parent').equals(this.id).toArray()
      ])
      return promise
        .then(([sections, paragraphs]) => {
          let ret: DtpSectionDtpModelInterface[] = sections.map(section => new SectionDtpModel(section.content, this.dtpModel, section.order, this, section.id));
          ret = ret.concat(paragraphs.map(paragraph => new ParagraphModel(paragraph.content, this.dtpModel, paragraph.order, paragraph.tags, this, paragraph.id)))
          return ret;
        });
    }
    return [];
  }

  save(): Promise<void> {
    return this._db.transaction('rw', this._db.sectionDtpModels,async() => {
      // @ts-ignore
      this.id = await this._db.sectionDtpModels.put(<DtpSectionDtpModelInterface>this._serialize());
    });
  }

  async delete(): Promise<void> {
    return this._db.transaction('rw', this._db.sectionDtpModels, this._db.paragraphModels, async () => {
      if (this.id !== undefined) {
        await Promise.all((await this.getChildren()).map(child => child.delete()));
        this._db.sectionDtpModels.where('id').equals(this.id).delete();
      }
    });
  }

  private _serialize() {
    return {
      content: this.content,
      dtpModel: this.dtpModel.id,
      order: this.order,
      parent: this.parent?.id ?? undefined,
      id: this.id
    }
  }

  getTags(): readonly TagsEnum[] {
    return this._tags ?? [];
  }

  setTags(model: DtpSectionDtpModelInterface, tags?: TagsEnum[]): void {
    this._mapTags.set(model, tags ?? []);
    this._tags = [...new Set([...this._mapTags.values()].reduce((acc, val) => acc.concat(val), []))];
    if (this.parent && this.parent instanceof SectionDtpModel) {
      this.parent.setTags(this, this._tags);
    }
  }
}
