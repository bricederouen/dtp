import {DtpModelInterface} from "./interfaces/dtpModel.interface";
import {Database} from "../database/database";
import {SectionDtpModel} from "./section-dtp.model";
import {DtpSectionDtpModelInterface} from "./interfaces/dtpSectionDtpModel.interface";
import {ParagraphModel} from "./paragraph.model";

export class DtpModel implements DtpModelInterface {

  public title: string = '';
  public id: number | undefined;
  private _db = new Database();

  constructor(title: string, id?: number) {
    this.title = title;
    this.id = id;
  }

  save(): Promise<void> {
    return this._db.transaction('rw', this._db.dtpModels,async() => {
      this.id = await this._db.dtpModels.put(<DtpModelInterface>this._serialize());
    });
  }

  private _serialize() {
    return {
      title: this.title,
      id: this.id
    }
  }

  async getChildren(): Promise<DtpSectionDtpModelInterface[]>{
    if (this.id) {
      const promise = Promise.all([
        this._db.sectionDtpModels.where('dtpModel').equals(this.id).toArray(),
        this._db.paragraphModels.where('dtpModel').equals(this.id).toArray()
      ])
      return promise
        .then(([sections, paragraphs]) => {
          let ret: DtpSectionDtpModelInterface[] = sections.filter(section => section.parent === null || section.parent === undefined).map(section => new SectionDtpModel(section.content, this, section.order, undefined, section.id));
          ret = ret.concat(paragraphs.filter(paragraph => paragraph.parent === null || paragraph.parent === undefined).map(paragraph => new ParagraphModel(paragraph.content, this, paragraph.order, paragraph.tags, undefined, paragraph.id)))
          return ret;
        });
    }
    return [];
  }

  async delete(): Promise<void> {
    return this._db.transaction('rw', this._db.dtpModels, this._db.sectionDtpModels, this._db.paragraphModels, async () => {
      if (this.id !== undefined) {
        this._db.dtpModels.where('id').equals(this.id).delete();
        this._db.sectionDtpModels.where('dtpModel').equals(this.id).delete();
        this._db.paragraphModels.where('dtpModel').equals(this.id).delete();
      }
    });
  }

}
